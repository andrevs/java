package exceptions.criteria.amountOfMoneyCriteria;

import exceptions.terminalExceptions.InvalidAmountOfMoneyException;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class DivisibleByOneHundred implements AmountOfMoneyCriterion {
    @Override
    public void check(int amount) {
        if (amount % 100 != 0)
            throw new InvalidAmountOfMoneyException("Amount of money should be divisible by 100!");
    }

}
