package exceptions.criteria.amountOfMoneyCriteria;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public interface AmountOfMoneyCriterion {
    void check(int amount);
}
