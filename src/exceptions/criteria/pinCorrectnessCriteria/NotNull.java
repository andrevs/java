package exceptions.criteria.pinCorrectnessCriteria;

import exceptions.terminalExceptions.IncorrectPinCodeException;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class NotNull implements PinCorrectnessCriterion {
    @Override
    public void check(String pin) {
        if (pin == null)
            throw new IncorrectPinCodeException("Pin code should not be null!");
    }

}
