package exceptions.criteria.pinCorrectnessCriteria;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public interface PinCorrectnessCriterion {
    void check(String pin);
}
