package exceptions.criteria.pinCorrectnessCriteria;

import exceptions.terminalExceptions.IncorrectPinCodeException;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class NotLessThanFourDigits implements PinCorrectnessCriterion {
    @Override
    public void check(String pin) {
        if (pin.length() < 4)
            throw new IncorrectPinCodeException("Pin code length should not be less than 4!");
    }

}
