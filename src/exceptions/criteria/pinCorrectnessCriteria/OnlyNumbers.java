package exceptions.criteria.pinCorrectnessCriteria;

import exceptions.terminalExceptions.IncorrectPinCodeException;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class OnlyNumbers implements PinCorrectnessCriterion {
    @Override
    public void check(String pin) {
        if (!pin.matches("[0-9]*"))
            throw new IncorrectPinCodeException("Pin code should contain only numbers!");
    }

}
