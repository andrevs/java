package exceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class Main {
    public static void main(String[] args) {
        AmountOfMoneyChecker amountOfMoneyChecker = makeAmountOfMoneyChecker();
        PinCorrectnessChecker pinCorrectnessChecker = makePinCorrectnessChecker();
        UserAccount userAccount = new UserAccount(1, "1234", pinCorrectnessChecker);
        TerminalServer terminalServer = new TerminalServerImpl(userAccount, amountOfMoneyChecker);
        PinValidator pinValidator = new PinValidatorImpl(userAccount, pinCorrectnessChecker, 5, 3);
        Terminal terminal = new TerminalImpl(terminalServer, pinValidator);
    }

    private static AmountOfMoneyChecker makeAmountOfMoneyChecker() {
        return new AmountOfMoneyCheckerImpl();
    }

    private static PinCorrectnessChecker makePinCorrectnessChecker() {
        return new PinCorrectnessCheckerImpl();
    }

}
