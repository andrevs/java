package exceptions;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public interface PinValidator {
    void validate(String pin);

}
