package exceptions;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public interface Terminal extends TerminalServer {
    void enterPin(String pin);
}
