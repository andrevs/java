package exceptions;

import exceptions.terminalExceptions.TerminalException;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public interface ExceptionsHandler {
    void handleException(TerminalException terminalException);

}
