package exceptions;

import exceptions.terminalExceptions.UnauthorizedAccessException;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class TerminalImpl implements Terminal {
    private final TerminalServer terminalServer;
    private final PinValidator pinValidator;
//    private ExceptionsHandler exceptionsHandler;

    private boolean userIsAuthorized = false;


    public TerminalImpl(TerminalServer terminalServer, PinValidator pinValidator) {
        this.terminalServer = terminalServer;
        this.pinValidator = pinValidator;
    }

    @Override
    public void enterPin(String pin) {
        if (!userIsAuthorized) {
            pinValidator.validate(pin);
            userIsAuthorized = true;
        }

    }

    @Override
    public int getMoney() {
        checkLockStatus();
        return terminalServer.getMoney();

    }

    @Override
    public int putMoney(int amount) {
        checkLockStatus();
        return terminalServer.putMoney(amount);

    }

    @Override
    public int withdrawMoney(int amount) {
        checkLockStatus();
        return terminalServer.withdrawMoney(amount);

    }

    private void checkLockStatus() {
        if (!userIsAuthorized)
            throw new UnauthorizedAccessException("Authorize before any actions with terminal!");

    }

}
