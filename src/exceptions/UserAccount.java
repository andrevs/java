package exceptions;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class UserAccount {
    private final int id;
    private String pin;
    private int amountOfMoney = 0;


    public UserAccount(int id, String pin, PinCorrectnessChecker pinCorrectnessChecker) {
        this.id = id;

        pinCorrectnessChecker.check(pin);
        this.pin = pin;

    }

    public UserAccount(int id, String pin, int amountOfMoney) {
        this.id = id;
        this.pin = pin;
        this.amountOfMoney = amountOfMoney;

    }

    public int getId() {
        return id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

}
