package exceptions;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public interface TerminalServer {
    int getMoney();
    int putMoney(int amount);
    int withdrawMoney(int amount);
}
