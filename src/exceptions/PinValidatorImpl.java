package exceptions;

import exceptions.terminalExceptions.AccountIsLockedException;
import exceptions.terminalExceptions.InvalidPinCodeException;
import exceptions.terminalExceptions.TerminalException;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class PinValidatorImpl implements PinValidator {
    private final UserAccount userAccount;
    private PinCorrectnessChecker pinCorrectnessChecker = new PinCorrectnessCheckerImpl();

    private int maxDeniedAttempts = 3;
    private int curDeniedAttempts = 0;
    private int lockDurationInSeconds = 5;
    private long lockTime;
    private boolean isLocked = false;

    public PinValidatorImpl(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public PinValidatorImpl(UserAccount userAccount, PinCorrectnessChecker pinCorrectnessChecker) {
        this.userAccount = userAccount;
        this.pinCorrectnessChecker = pinCorrectnessChecker;
    }

    public PinValidatorImpl(UserAccount userAccount, int lockDurationInSeconds, int maxDeniedAttempts) {
        this.userAccount = userAccount;
        this.lockDurationInSeconds = lockDurationInSeconds;
        this.maxDeniedAttempts = maxDeniedAttempts;
    }

    public PinValidatorImpl(UserAccount userAccount, PinCorrectnessChecker pinCorrectnessChecker, int lockDurationInSeconds, int maxDeniedAttempts) {
        this.userAccount = userAccount;
        this.pinCorrectnessChecker = pinCorrectnessChecker;
        this.lockDurationInSeconds = lockDurationInSeconds;
        this.maxDeniedAttempts = maxDeniedAttempts;
    }

    @Override
    public void validate(String pin) {
        if (this.isLocked())
            throw new AccountIsLockedException(String.format(
                    "Account is locked. Wait %d seconds.", getRemainingLockDurationInSeconds()));

        try {
            pinCorrectnessChecker.check(pin);
        } catch (TerminalException e) {
            handleDeniedAttempt();
            throw e;
        }

        if (!pin.equals(userAccount.getPin())) {
            handleDeniedAttempt();
            throw new InvalidPinCodeException("Invalid pin code!");
        }

    }

    private void handleDeniedAttempt() {
        ++curDeniedAttempts;

        if (curDeniedAttempts >= maxDeniedAttempts) {
            isLocked = true;
            curDeniedAttempts = 0;
            lockTime = new Date().getTime();
        }

    }

    public PinCorrectnessChecker getPinCorrectnessChecker() {
        return pinCorrectnessChecker;
    }

    public void setPinCorrectnessChecker(PinCorrectnessChecker pinCorrectnessChecker) {
        this.pinCorrectnessChecker = pinCorrectnessChecker;
    }

    public int getLockDurationInSeconds() {
        return lockDurationInSeconds;
    }

    public boolean isLocked() {
        if (this.getRemainingLockDurationInSeconds() == 0)
            isLocked = false;

        return isLocked;

    }

    public long getRemainingLockDurationInSeconds() {
        if (isLocked) {
            long curTime = new Date().getTime();
            return Math.max(0, lockDurationInSeconds - TimeUnit.SECONDS.toSeconds(curTime - lockTime));
        } else {
            return 0;
        }

    }

}
