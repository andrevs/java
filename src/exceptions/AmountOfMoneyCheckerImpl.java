package exceptions;

import exceptions.criteria.amountOfMoneyCriteria.AmountOfMoneyCriterion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class AmountOfMoneyCheckerImpl implements AmountOfMoneyChecker {
    List<AmountOfMoneyCriterion> criteria = new ArrayList<>();

    public AmountOfMoneyCheckerImpl() {
    }

    public AmountOfMoneyCheckerImpl(List<AmountOfMoneyCriterion> list) {
        this.criteria = list;
    }

    public void addCriterion(AmountOfMoneyCriterion criterion) {
        criteria.add(criterion);
    }

    @Override
    public void check(int amount) {
        for(AmountOfMoneyCriterion criterion : criteria)
            criterion.check(amount);
    }

}
