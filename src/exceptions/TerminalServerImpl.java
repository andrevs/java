package exceptions;

import exceptions.terminalExceptions.NoConnectionException;
import exceptions.terminalExceptions.NotEnoughMoneyException;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class TerminalServerImpl implements TerminalServer {
    private final UserAccount userAccount;
    private AmountOfMoneyChecker amountOfMoneyChecker = new AmountOfMoneyCheckerImpl();

    public TerminalServerImpl(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public TerminalServerImpl(UserAccount userAccount, AmountOfMoneyChecker amountOfMoneyChecker) {
        this.userAccount = userAccount;
        this.amountOfMoneyChecker = amountOfMoneyChecker;

    }

    @Override
    public int getMoney() {
        // easter egg
        if (userAccount.getAmountOfMoney() == 666)
            throw new NoConnectionException("No connection. Try to change amount of money.");

        return userAccount.getAmountOfMoney();

    }

    @Override
    public int putMoney(int amount) {
        amountOfMoneyChecker.check(amount);

        userAccount.setAmountOfMoney(userAccount.getAmountOfMoney() + amount);

        return userAccount.getAmountOfMoney();

    }

    @Override
    public int withdrawMoney(int amount) {
        amountOfMoneyChecker.check(amount);

        if (userAccount.getAmountOfMoney() < amount)
            throw new NotEnoughMoneyException("Not enough money to withdraw requested amount!");

        userAccount.setAmountOfMoney(userAccount.getAmountOfMoney() - amount);

        return userAccount.getAmountOfMoney();

    }

}
