package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class UnauthorizedAccessException extends TerminalException {
    public UnauthorizedAccessException(String message) {
        super(message);
    }

}
