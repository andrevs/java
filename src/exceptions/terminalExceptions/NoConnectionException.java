package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class NoConnectionException extends TerminalException {
    public NoConnectionException(String message) {
        super(message);
    }

}
