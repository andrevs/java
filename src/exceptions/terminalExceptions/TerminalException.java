package exceptions.terminalExceptions;

import exceptions.Terminal;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class TerminalException extends RuntimeException {
    public TerminalException(String message) {
        super(message);
    }

}
