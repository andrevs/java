package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class InvalidAmountOfMoneyException extends TerminalException {
    public InvalidAmountOfMoneyException(String message) {
        super(message);
    }

}
