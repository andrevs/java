package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class AccountIsLockedException extends TerminalException {
    public AccountIsLockedException(String message) {
        super(message);
    }

}
