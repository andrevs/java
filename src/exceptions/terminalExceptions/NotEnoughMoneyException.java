package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class NotEnoughMoneyException extends TerminalException {
    public NotEnoughMoneyException(String message) {
        super(message);
    }

}
