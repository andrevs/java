package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class IncorrectPinCodeException extends TerminalException {
    public IncorrectPinCodeException(String message) {
        super(message);
    }

}
