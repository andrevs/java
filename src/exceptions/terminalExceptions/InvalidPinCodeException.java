package exceptions.terminalExceptions;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class InvalidPinCodeException extends TerminalException {
    public InvalidPinCodeException(String message) {
        super(message);
    }

}
