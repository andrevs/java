package exceptions;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public interface PinCorrectnessChecker {
    void check(String pin);
}
