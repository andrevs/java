package exceptions;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public interface AmountOfMoneyChecker {
    void check(int amount);
}
