package exceptions;

import exceptions.terminalExceptions.TerminalException;

import java.io.PrintStream;

/**
 * Created by Vasyukevich Andrey on 20.11.2016.
 */
public class ExceptionsHandlerImpl implements ExceptionsHandler {
    private final PrintStream printStream;

    public ExceptionsHandlerImpl(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void handleException(TerminalException terminalException) {
        printStream.println(terminalException.getMessage());
    }

}
