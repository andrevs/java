package exceptions;

import exceptions.criteria.pinCorrectnessCriteria.PinCorrectnessCriterion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasyukevich Andrey on 21.11.2016.
 */
public class PinCorrectnessCheckerImpl implements PinCorrectnessChecker {
    List<PinCorrectnessCriterion> criteria = new ArrayList<>();

    public PinCorrectnessCheckerImpl() {
    }

    public PinCorrectnessCheckerImpl(List<PinCorrectnessCriterion> list) {
        this.criteria = list;
    }

    public void addCriterion(PinCorrectnessCriterion criterion) {
        criteria.add(criterion);
    }

    @Override
    public void check(String pin) {
        for(PinCorrectnessCriterion criterion : criteria)
            criterion.check(pin);

    }

}
