package temp;

import java.util.Objects;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class Temp {
    public static void main(String[] args) {
        Class<?> clazz = Object.class;
        System.out.println(clazz);
        System.out.println(clazz.getSuperclass());
    }
}
