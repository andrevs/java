package threadpool;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class ThreadFunc implements Runnable {
    List<Runnable> runnables;
    private AtomicBoolean stop;
    private long timeout = 1000;

    public ThreadFunc(List<Runnable> runnables, AtomicBoolean stop) {
        this.runnables = runnables;
        this.stop = stop;
    }

    @Override
    public void run() {
        while (!stop.get()) {
            Runnable runnable = null;

            synchronized (runnables) {
                while(!stop.get() && runnables.isEmpty()) {
                    try {
                        runnables.wait(timeout);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

                if (!runnables.isEmpty()) {
                    runnable = runnables.get(0);
                    runnables.remove(0);

                }
            }

            if (runnable != null)
                runnable.run();

        }
    }

}
