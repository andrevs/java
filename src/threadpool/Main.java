package threadpool;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class Main {
    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool();
        threadPool.start(4);

        for (int i = 0; i < 100; i++) {
            final int finalI = i;
            threadPool.execute(() -> {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.format("%d %d\n", Thread.currentThread().getId(), finalI);

            });
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        threadPool.stop();
        threadPool.join();

    }
}
