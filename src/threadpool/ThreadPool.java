package threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class ThreadPool {
    private List<Runnable> runnables = new ArrayList<>();
    private List<Thread> threads = new ArrayList<>();

    private AtomicBoolean stop = new AtomicBoolean(false);

    public void start(int count) {
        for (int i = 0; i < count; i++)
            threads.add(new Thread(new ThreadFunc(runnables, stop)));

        threads.forEach(Thread::start);

    }

    public void join() {
        for(Thread thread : threads)
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }

    public void stop() {
        stop.set(true);

        synchronized (runnables) {
            runnables.notifyAll();
        }

    }

    public void execute(Runnable runnable) {
        synchronized (runnables) {
            runnables.add(runnable);
            runnables.notify();
        }

    }

}
