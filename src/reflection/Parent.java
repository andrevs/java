package reflection;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class Parent {
    private String name;

    public Parent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
