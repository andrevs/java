package reflection;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class Main {
    public static void main(String[] args) {
        From from = new From();
        To to = new To();

        System.out.println(from.getField().getName());
        System.out.println(to.getField().getName());

        BeanUtils.assign(to, from);

        System.out.println(from.getField().getName());
        System.out.println(to.getField().getName());
        
    }
}
