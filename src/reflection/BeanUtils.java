package reflection;

// http://tutorials.jenkov.com/java-reflection/index.html
// http://restless-man.livejournal.com/24320.html

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class BeanUtils {
    /**
     * Scans object "from" for all getters. If object "to"
     * contains correspondent setter, it will invoke it
     * to set property value for "to" which equals to the property
     * of "from".
     * <p/>
     * The type in setter should be compatible to the value returned
     * by getter (if not, no invocation performed).
     * Compatible means that parameter type in setter should
     * be the same or be superclass of the return type of the getter.
     * <p/>
     * The method takes care only about public methods.
     *
     * @param to   Object which properties will be set.
     * @param from Object which properties will be used to get values.
     */
    public static void assign(Object to, Object from) {

        for (Method fromGetter : from.getClass().getMethods()) {
            if (isGetter(fromGetter)) {
                Method toSetter = getCorrespondingSetter(to.getClass(), fromGetter);
                if (toSetter != null) {
                    try {
                        Object value = fromGetter.invoke(from);
                        toSetter.invoke(to, value);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    private static boolean isGetter(Method method) {
        boolean startsWithGet = method.getName().startsWith("get");
        if (!startsWithGet)
            return false;

        boolean hasNoParameters = (method.getParameterCount() == 0);
        if (!hasNoParameters)
            return false;

        boolean hasReturningValue = (method.getReturnType() != void.class);
        if (!hasReturningValue)
            return false;

        return true;

    }

    private static Method getCorrespondingSetter(Class<?> clazz, Method method) {
        if (!isGetter(method))
            return null;

        String name = method.getName().replaceFirst("get", "set");
        Class<?> parameterType = method.getReturnType();

        Method setter = null;

        while (parameterType != null) {
            try {
                setter = clazz.getMethod(name, parameterType);
                break;
            } catch (NoSuchMethodException e) {
                parameterType = parameterType.getSuperclass();
            }
        }

        return setter;

    }

}
