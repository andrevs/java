package reflection;

/**
 * Created by Vasyukevich Andrey on 14.11.2016.
 */
public class From {
    private Child field = new Child("from_parent");

    public Child getField() {
        return field;
    }

    public void setField(Child field) {
        this.field = field;
    }
}
