package reflection;

/**
 * Created by User22 on 14.11.2016.
 */
public class To {
    private Parent field = new Parent("to_child");

    public Parent getField() {
        return field;
    }

    public void setField(Parent field) {
        this.field = field;
    }

}
