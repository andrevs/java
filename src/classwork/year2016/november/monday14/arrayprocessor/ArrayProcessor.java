package classwork.year2016.november.monday14.arrayprocessor;

/**
 * Created by User22 on 14.11.2016.
 */
public interface ArrayProcessor {
    int[] process(int[] original, ElementStrategy strategy, int threadCount);

}
