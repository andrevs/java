package classwork.year2016.november.monday14.arrayprocessor;

/**
 * Created by User22 on 14.11.2016.
 */
public interface ElementStrategy {
    int process(int value);

}
