package classwork.year2016.november.monday14.arrayprocessor;

/**
 * Created by User22 on 14.11.2016.
 */
public class AddConstant implements ElementStrategy {
    private int constant;

    public AddConstant(int constant) {
        this.constant = constant;
    }

    @Override
    public int process(int value) {
        return value + constant;
    }

}
