package classwork.year2016.november.monday14.arrayprocessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by User22 on 14.11.2016.
 */
public class ArrayProcessorImpl implements ArrayProcessor {
    @Override
    public int[] process(int[] original, ElementStrategy strategy, int threadCount) {
        int numberOfThreads = Math.min(original.length, threadCount);

        List<Thread> threads = new ArrayList<>();
        List<int[]> parts = new ArrayList<>();

        for (int i = 0; i < numberOfThreads; i++) {
            int[] part = getPart(original, i, numberOfThreads);
            parts.add(part);
            Thread thread = new Thread(new ThreadFunc(part, strategy));
            threads.add(thread);
        }

        for (int i = 0; i < numberOfThreads; i++)
            threads.get(i).start();

        for (int i = 0; i < numberOfThreads; i++) {
            try {
                threads.get(i).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int[] result = new int[original.length];

        for (int i = 0; i < numberOfThreads; i++)
            fillPart(result, i, numberOfThreads, parts.get(i));

        return result;

    }

    private static int[] getPart(int[] data, int i, int n) {
        return Arrays.copyOfRange(data, partBegin(data.length, i, n), partEnd(data.length, i, n));
    }

    private static void fillPart(int[] data, int i, int n, int[] source) {
        int begin = partBegin(data.length, i, n);
        int end = partEnd(data.length, i, n);
        for (int j = 0; j < end - begin; ++j)
            data[begin + j] = source[j];
    }

    private static int partLength(int length, int i, int n) {
        return length / n + Math.min(1, length % n);
    }

    private static int partBegin(int length, int i, int n) {
        return partLength(length, i, n) * i;
    }

    private static int partEnd(int length, int i, int n) {
        return Math.min(length, partLength(length, i, n) * (i + 1));
    }

    private class ThreadFunc implements Runnable {
        private final int[] data;
        private final ElementStrategy elementStrategy;

        public ThreadFunc(int[] data, ElementStrategy elementStrategy) {
            this.data = data;
            this.elementStrategy = elementStrategy;
        }

        public int[] getData() {
            return data;
        }

        @Override
        public void run() {
            for (int i = 0; i < data.length; i++)
                data[i] = elementStrategy.process(data[i]);
        }

    }

}
