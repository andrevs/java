package classwork.year2016.november.monday14.arrayprocessor;

import java.util.Arrays;

/**
 * Created by User22 on 14.11.2016.
 */
public class Main {
    public static void main(String[] args) {
        int n = 10;
        int[] data = new int[n];

        for (int i = 0; i < n; i++)
            data[i] = i + 1;

        ArrayProcessor arrayProcessor = new ArrayProcessorImpl();

        System.out.println(Arrays.toString(data));

        int[] result = arrayProcessor.process(data, new AddConstant(9), 11);

        System.out.println(Arrays.toString(result));

    }

}
